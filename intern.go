package chain

type pool map[string]string

// Uses string interning to reduce memory usage when reading large texts
// Neat and probably not needed, learned from https://artem.krylysov.com/blog/2018/12/12/string-interning-in-go/
func (p pool) stringIntern(token string) string {
	if token, ok := p[token]; ok {
		return token
	}
	p[token] = token
	return token
}
