# What
A text based markov chain generator in go. Probably can be done in much less steps than this, but was a fun exercise.

# How to use example cli
Provided is a simple cli program (cmd/main.go) to interact with the library
To build enter the "cmd" directory and "go install" or "go build"

~~~
Usage:
  -file value
      Files to be used. Use -file<file1> -file<file2> for multiple [Required]
  -gram_size int
      Length of the word or char gram (default 3)
  -text_size int
      Length of the text to build (default 150)
  -type string
      Type of chain to build { word | char } (default "word")
~~~

A word chain builds ngrams of words, a char chain builds ngrams on words, based on gram_size

To get an idea of how the text is built, with gram_size of 4, and type_string 'word' the ngram map looks like
~~~
the quick brown fox [jumped hopped]
~~~

gram_size of 2, and type_string 'char' looks like
~~~
t h [e i a]
~~~

Example command and output
~~~
[master●] » gomark -file texts/Dickens-AChristmasCarol.txt -type word -text_size=100 -gram_size=3

Word limit of 100:
-------------------------
Any Cratchit would have blushed to hint at such a 

 At last the dishes were set on, and grace was said. It was succeeded by a breathless pause, as Mrs. Cratchit, looking slowly all along the carving-knife, prepared to plunge it in the breast; but when she did, and when the long-expected gush of stuffing issued forth, one murmur of delight arose all round the board, and even Tiny Tim, excited by the two young Cratchits got upon his knees, and laid, each child, a little cheek against his face, as if they said, "Don't mind it, father.

 Character limit of 100 : 
-------------------------
Long life to him! A merry Christmas to us all, my dears. God bless us!" 

 Which all the family re-echoed.
~~~

# API methods

See chain/chain.go for full exported methods, or "API Usage" below for how to use the library

# API Usage

Below is an example program using the api
See 'godoc gomark/chain' for full api with comments

```go
import (
    "fmt"
    "os"
    "gomark/chain"
)

ngram_size := 2
ngram_type := "word" 
gmap := chain.NewGramMap(ngram_size, ngram_type)

ListOfText := [text1.txt, text2.txt]

for _ , file := range ListOfText {
  err := gmap.BuildMap(string(file))
  if err != nil{
    fmt.Println(err)
  }
}
// gmap is now populated and ready to generate chains

// Export and import the populated GramMap
var location_to_save string = "savedgmap.gob"
chain.SaveMap(gmap, location_to_save)

var location_of_gmap string = "savedgmap.gob"
gmap = chain.LoadMap(location_of_gmap)

// Generate some text

// How many words the text should have
text_length := 100
// How many times should we try generating text
tries := 100

// Generate text from corpus with a word limit, and character limit
builtTextWordLimit, err := gmap.BuildTextWordLimit(text_length, tries)
builtTextCharLimit, err2 := gmap.BuildTextCharLimit(text_length, tries)

if err == nil && err2 == nil{
  fmt.Println(builtTextWordLimit)
  fmt.Println(builtTextCharLimit)
}
```

# Assumptions
This will always try to build a text with an uppercase start, and an ending with a valid ending punctuation (. ! ?). If this is not possible with the input in the number of tries, an error will be returned.
