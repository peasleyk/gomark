package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	chain "gitlab.com/peasleyk/gomark"
)

// Small example cli program using chain.go

// Neat trick from https://stackoverflow.com/q/28322997/
// If I continue using go for anything, definitely use a different flag parser...
type flagArray []string

func (i *flagArray) String() string {
	return ""
}

func (i *flagArray) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func main() {
	rand.Seed(time.Now().UnixNano())

	var fileArr flagArray
	flag.Var(&fileArr, "file", "Files to be used. Use -file<file1> -file<file2> for multiple [Required]")
	textSizePtr := flag.Int("text_size", 150, "Length of the text to build")
	gramSizePtr := flag.Int("gram_size", 3, "Length of the word or char gram")
	splitTypePtr := flag.String("type", "word", "Type of chain to build { word | char }")
	flag.Parse()

	if len(fileArr) == 0 {
		flag.PrintDefaults()
		os.Exit(1)
	}

	gmap, _ := chain.NewGramMap(*gramSizePtr, *splitTypePtr)
	//Populate our gram map
	for _, file := range fileArr {
		err := gmap.BuildMapFromFile(string(file))
		if err != nil {
			fmt.Println(err)
		}
	}

	// Build a text with a word and character limit
	builtTextW, err := gmap.BuildTextWordLimit(*textSizePtr, 100)
	builtTextC, err2 := gmap.BuildTextCharLimit(*textSizePtr, 100)

	fmt.Printf("Word limit of %v:\n", *textSizePtr)
	fmt.Println("-------------------------")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(builtTextW)
	}

	fmt.Printf("\n Character limit of %v : \n", *textSizePtr)
	fmt.Println("-------------------------")
	if err2 != nil {
		fmt.Println(err2)
	} else {
		fmt.Println(builtTextC)
	}

}
