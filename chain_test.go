package chain

import (
	"reflect"
	"strconv"
	"testing"
	"unsafe"
)

func stringptr(s string) uintptr {
	return (*reflect.StringHeader)(unsafe.Pointer(&s)).Data
}

func TestInterning(t *testing.T) {

	p := pool{}
	one := p.stringIntern("42")
	two := p.stringIntern(strconv.Itoa(42))

	if stringptr(one) != stringptr(two) {
		t.Error("Strings should have same pointer")
	}
}

func TestCreating(t *testing.T) {
	ngramSize := 1
	ngramType := "word"
	gmap, err := NewGramMap(ngramSize, ngramType)
	if err != nil {
		t.Errorf("Gram is valid")
	}
	err = gmap.BuildMapFromFile("texts/Dickens-AChristmasCarol.txt")
	if err != nil {
		t.Errorf("Text is valid")
	}
}

func TestCreatingZero(t *testing.T) {
	ngramSize := 0
	ngramType := "word"
	_, err := NewGramMap(ngramSize, ngramType)
	if err != nil {
		t.Skip()
	}
	t.Errorf("Should have failed on 0 gram size")
}

func TestCreatingInvalidType(t *testing.T) {
	ngramSize := 4
	ngramType := "empty"
	_, err := NewGramMap(ngramSize, ngramType)
	if err != nil {
		t.Skip()
	}
	t.Errorf("Should have failed on 'empty' gram type")
}

func createOutput(s int, ty string, t *testing.T) {
	ngramType := ty
	text_size := s
	for ngramSize := 3; ngramSize < 4; ngramSize++ {
		gmap, err := NewGramMap(ngramSize, ngramType)
		if err != nil {
			t.Errorf("Gram should be valid: %v", err)
		}
		err = gmap.BuildMapFromFile("texts/Dickens-AChristmasCarol.txt")
		if err != nil {
			t.Errorf("Struct should have been built: %v", err)
		}
		_, err = gmap.BuildTextWordLimit(text_size, 100)
		if err != nil {
			t.Errorf("Should have run, err: %v", err)
		}
		_, err = gmap.BuildTextCharLimit(text_size, 100)
		if err != nil {
			t.Errorf("Should have run, err: %v", err)
		}
	}
}

func TestCreateCharOutput(t *testing.T) {
	createOutput(100, "char", t)
}

func TestCreateWordOutput(t *testing.T) {
	createOutput(100, "word", t)
}

func benchmarkCreate(s int, t string, b *testing.B) {
	ngramSize := s
	ngramType := t
	gmap, _ := NewGramMap(ngramSize, ngramType)
	for n := 0; n < b.N; n++ {
		_ = gmap.BuildMapFromFile("texts/Dickens-AChristmasCarol.txt")
	}
}

func BenchmarkWordCreate1(b *testing.B) {
	benchmarkCreate(1, "word", b)
}
func BenchmarkWordCreate2(b *testing.B) {
	benchmarkCreate(2, "word", b)
}
func BenchmarkWordCreate3(b *testing.B) {
	benchmarkCreate(3, "word", b)
}
func BenchmarkWordCreate4(b *testing.B) {
	benchmarkCreate(4, "word", b)
}
func BenchmarkCharCreate1(b *testing.B) {
	benchmarkCreate(1, "char", b)
}
func BenchmarkCharCreate2(b *testing.B) {
	benchmarkCreate(2, "char", b)
}
func BenchmarkCharCreate3(b *testing.B) {
	benchmarkCreate(3, "char", b)
}
func BenchmarkCharCreate4(b *testing.B) {
	benchmarkCreate(4, "char", b)
}

func benchmarkGenerate(s int, t string, l string, b *testing.B) {
	// run the Fib function b.N times
	// ngramSize := 1
	ngramSize := s
	ngramType := t
	text_size := 100
	gmap, _ := NewGramMap(ngramSize, ngramType)

	//nolint
	gmap.BuildMapFromFile("texts/Dickens-AChristmasCarol.txt")

	for n := 0; n < b.N; n++ {
		//nolint
		if l == "word" {
			_, _ = gmap.BuildTextWordLimit(text_size, 200)
		}
		if l == "char" {
			_, _ = gmap.BuildTextCharLimit(text_size, 200)

		}
	}
}

func BenchmarkWordforCharLimit(b *testing.B) {
	benchmarkGenerate(2, "word", "char", b)
}

func BenchmarkCharforCharLimit(b *testing.B) {
	benchmarkGenerate(3, "char", "char", b)
}

func BenchmarkWordforWordLimit(b *testing.B) {
	benchmarkGenerate(2, "word", "word", b)
}

func BenchmarkCharforWorLimit(b *testing.B) {
	benchmarkGenerate(3, "char", "word", b)
}
