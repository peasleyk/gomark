package chain

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	CHAR = ""
	WORD = " "
)

// Removes the first element (word or char) from an array and appends a new
func addWord(bag []string, word string) []string {
	bag = bag[1:]
	bag = append(bag, word)
	return bag

}

// Reads in a file and transforms it into a large string array, preserving newlines.
// Returns that array or an error in reading
// Scanner is used to remove newlines from lines. Otherwise ioutil.ReadFile will preserve the original style
func scanFiles(fileName string, gramType string) ([]string, error) {
	_, err := os.Stat(fileName)
	if err != nil {
		return nil, fmt.Errorf("File %s does not exist", fileName)
	}
	file, err := os.Open(fileName)
	if err != nil {
		return nil, (err)
	}
	var lines []string
	scanner := bufio.NewScanner(file)
	p := pool{}
	for scanner.Scan() {
		text := strings.TrimSpace(scanner.Text())
		wordArray := strings.Split(text, gramType)
		if len(wordArray) > 1 {
			for _, word := range wordArray {
				lines = append(lines, p.stringIntern(word))
			}
		} else {
			lines = append(lines, "\n\n")
		}
	}
	file.Close()
	return lines, nil
}

// GramMap hold the gram maps, as well as type information
type GramMap struct {
	ChainMap map[string][]string
	Keys     []string
	GramSize int
	GramType string
}

// Gets a random key entry in our GramMap
// Useful or when we start building the text, and when we hit the end of the map while building
// Ensures the first word int he gram starts with a capital letter
func (gmap *GramMap) getRandGram() (string, error) {
	rKey := rand.Intn(len(gmap.ChainMap))
	for rKey < len(gmap.Keys) {
		key := gmap.Keys[rKey]
		if key != "" {
			// We want to choose the start of a sentence
			r := rune(key[0])
			if unicode.IsUpper(r) {
				return key, nil
			}
		}
		rKey++
	}
	// If we don't have any capital letters
	return "", errors.New("No more keys to get")
}

func lengthCheck(s string, m string, t string) int {
	counter := 0
	if m == "word" {
		if !strings.Contains(s, "\n") && !strings.Contains(s, "\t") {
			counter++
		}
	}
	if m == "char" {
		if s != "\n" {
			counter = utf8.RuneCountInString(s)
			if t == WORD {
				counter++
			}
		}
	}
	return counter
}

// Takes in a GramMap object, the length to build to, and the measure (word limit, char limit)
// Outputs the generated text
func (gmap *GramMap) generateText(textLength int, measure string, triesLimit int) (string, error) {
	if len(gmap.ChainMap) == 0 {
		return "", errors.New("Empty ChainMap in gmap, nothing to build upon")
	}
	startToken, err := gmap.getRandGram()
	if err != nil {
		return "", err
	}

	p := pool{}

	// Add the initial content to the output
	// Wordbag is the holder of the current ngram
	var sBuilder strings.Builder
	ngramBag := make([]string, gmap.GramSize)
	for _, word := range strings.Split(startToken, gmap.GramType) {
		ngramBag = addWord(ngramBag, p.stringIntern(word))
		if gmap.GramType == WORD {
			sBuilder.WriteString(p.stringIntern(word) + " ")
		} else {
			sBuilder.WriteString(p.stringIntern(word))
		}
	}

	// Start generating the rest of the output
	counter := 0
	for counter < textLength {
		var wordChoice string
		choices := gmap.ChainMap[strings.Join(ngramBag, gmap.GramType)]
		if len(choices) > 0 {
			chosenWord := rand.Intn(len(choices))
			wordChoice = choices[chosenWord]
			ngramBag = addWord(ngramBag, p.stringIntern(wordChoice))
		} else {
			// We ran out of choices - end of the road for this seed
			// Pick a random starting point
			return "", errors.New("No more entries next")
		}

		if gmap.GramType == WORD {
			sBuilder.WriteString(p.stringIntern(wordChoice) + " ")
		} else {
			sBuilder.WriteString(p.stringIntern(wordChoice))
		}

		count := lengthCheck(p.stringIntern(wordChoice), measure, gmap.GramType)
		counter += count
	}

	// Once we have an output, let's see if we have a good ending
	generatedText := strings.TrimSpace(sBuilder.String())
	if !hasEndingPunctuation(generatedText, gmap.GramType) {
		return "", errors.New("Text has no ending punctuation")
	}

	return generatedText, nil
}

// Checks if the generated text has an ending
func hasEndingPunctuation(text string, gType string) bool {
	// return true
	endings := []string{".", "?", "!"}
	for _, e := range endings {

		if strings.HasSuffix(text, e) {
			return true
		}
	}
	return false
}

// BuildMapFromFile builds a word pool from a file
// Takes in a location to a text file to build the model with, returns error if error
func (gmap *GramMap) BuildMapFromFile(fileName string) error {
	if gmap.GramSize <= 0 {
		return errors.New("GramSize must be larger than 0")
	}
	text, err := scanFiles(fileName, gmap.GramType)
	if err != nil {
		return err
	}
	gmap.buildMap(text)
	return nil
}

// BuildMapFromText builds a GramMap from a reader
// Takes in a io.Reader with text, returns error if error
// This is a mess
func (gmap *GramMap) BuildMapFromText(reader io.Reader) error {
	if gmap.GramSize <= 0 {
		return errors.New("GramSize must be larger than 0")
	}
	b := new(bytes.Buffer)
	_, err := b.ReadFrom(reader)
	if err != nil {
		return err
	}
	text := b.String()
	sText := strings.Split(text, gmap.GramType)
	gmap.buildMap(sText)
	return nil
}

// Populated the gram from an array of strings
func (gmap *GramMap) buildMap(text []string) {
	wordBag := make([]string, gmap.GramSize)
	p := pool{}
	for _, word := range text {
		key := strings.Join(wordBag, gmap.GramType)
		if _, ok := gmap.ChainMap[key]; !ok {
			gmap.Keys = append(gmap.Keys, key)
		}
		gmap.ChainMap[key] = append(gmap.ChainMap[key], p.stringIntern(word))
		wordBag = addWord(wordBag, word)
	}
}

// BuildTextWordLimit attempts to build a text body given the gramType, gramSize, and the number of words equal to textLength.
// This function ensures the beginning of the text is a capital, and the end is a sentence.
// If tries are exhausted, an error is returned. If not returns the text strings.
func (gmap *GramMap) BuildTextWordLimit(textLength int, triesLimit int) (string, error) {
	if gmap.GramType == CHAR {
		// this will possibly blow up
	}
	output, err := gmap.buildText(textLength, triesLimit, "word")
	if err != nil {
		return "", err
	}
	return output, nil
}

// BuildTextCharLimit attempts to build a text body given the gramType, gramSize, and the number of chars equal to textLength.
// This function ensures the beginning of the text is a capital, and the end is a sentence.
// If tries are exhausted, an error is returned. If not the text string is.
func (gmap *GramMap) BuildTextCharLimit(textLength int, triesLimit int) (string, error) {
	output, err := gmap.buildText(textLength, triesLimit, "char")
	if err != nil {
		return "", err
	}
	return output, nil

}

// TODO - punctuation option - removed for now. Should add flag, then check for all, not just full stop
// Internal method to handle gram types when building text
func (gmap *GramMap) buildText(textLength int, triesLimit int, textType string) (string, error) {
	var err error
	var text string
	for i := 0; i < triesLimit; i++ {
		text, err = gmap.generateText(textLength, textType, triesLimit)
		if err == nil {
			return text, nil
		}
	}
	return "", fmt.Errorf("Ran out of tries generating: %v", err)
}

// NewGramMap creates a GramMap struct to be operated on.
// gramSize is the size of the ngram.
// gramType is the type, word or char e.g. with gramSize at 2.
// word: the quick [brown lazy yellow],
// char: t h [e i a]
func NewGramMap(gramSize int, gramType string) (*GramMap, error) {
	// Set splitType based on char or word
	var split string
	if gramType == "word" {
		split = WORD
	} else if gramType == "char" {
		split = CHAR
	} else {
		return nil, fmt.Errorf("Invalid gramtype '%s' provided", gramType)
	}

	if gramSize <= 0 {
		return nil, errors.New("ngram must be > 0")
	}

	gmap := GramMap{
		ChainMap: make(map[string][]string),
		GramSize: gramSize,
		GramType: split,
	}
	return &gmap, nil
}

// SaveGmap writes the GramMap struct returned from NewGramMap or LoadGMap to disk
func SaveGmap(gmap *GramMap, location string) error {
	file, err := os.Create(location)
	if err != nil {
		return err
	}
	encoder := gob.NewEncoder(file)
	err = encoder.Encode(gmap)
	if err != nil {
		return err
	}
	file.Close()
	return nil
}

// LoadGmap loads the GramMap struct returned from SaveGram to disk from location
func LoadGmap(location string) (*GramMap, error) {
	file, err := os.Open(location)
	if err != nil {
		return nil, err
	}
	gmap := GramMap{}
	decoder := gob.NewDecoder(file)
	err = decoder.Decode(&gmap)
	if err != nil {
		return nil, err
	}
	return &gmap, nil
}
