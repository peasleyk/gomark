//+build mage

// Mage build script for My blog. See options below
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Globals used in jobs below
var BINARY_NAME = "main"
var BUILD_DIR = ""
var BINARY_PATH = filepath.Join(BUILD_DIR, BINARY_NAME)
var Default = Build
var PROJECT_ID = "5757947"
var JOB_LIMIT = 5

// Response of gitlab jobs
type Job struct {
	Ref        string `json:"ref"`
	Created_at string `json:"created_at"`
	Name       string `json:"name"`
	Status     string `json:"status"`
}

// Builds the binary.
func Build() error {
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", "-o", BINARY_PATH, "cmd/main.go")
}

// Tests the library
func Test() error {
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "test")
}

// Test and run benchmarks
func Bench() error {
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "test", "-bench=.", "-benchmem")
}

// Run the build binary
func Run() error {
	return sh.RunV("./" + BINARY_PATH)
}

// Remove the temporarily generated files from Build.
func Clean() error {
	cmd := mg.GoCmd()
	err := sh.RunV(cmd, "clean")
	err = sh.Rm(BUILD_DIR)
	if err != nil {
		return err
	}
	return nil
}

// Go tools to run before committing
// This sticks to built in tooling for now
func Check() {
	cmd := mg.GoCmd()
	// Neat way to kick off an anonymous function
	defer func() {
		{
			sh.RunV(cmd, "fmt")
			sh.RunV(cmd, "vet")
			sh.RunV(cmd, "mod", "tidy")
			sh.RunV(cmd, "mod", "verify")
			sh.RunV("golangci-lint", "run")
			sh.RunV(cmd, "clean")
		}
	}()
}

func Open() {
	sh.RunV("firefox", "https://gitlab.com/peasleyk/gomark")
}

// Checks status of gitlab jobs
func Status() {
	jobs, err := getJobs()
	if err != nil {
		fmt.Printf("Erro gettings jobs: %v", err.Error())
		return
	}
	res := maxes(jobs)

	len_total := 0
	for _, v := range res {
		len_total += v
	}
	println("+---" + strings.Repeat("-", len_total+len(res)) + "----+")
	for x := 0; float64(x) < math.Min(float64(JOB_LIMIT), float64(len(jobs))); x++ {
		e := reflect.ValueOf(&jobs[x]).Elem()
		out := ""
		for i := 0; i < e.NumField(); i++ {
			name := e.Type().Field(i).Name
			value := e.Field(i).String()
			if i == e.NumField()-1 {
				out += fmt.Sprintf("| %-*s |", res[name], value)
			} else {
				out += fmt.Sprintf("| %-*s ", res[name], value)
			}
		}
		println(out)
	}
	println("+---" + strings.Repeat("-", len_total+len(res)) + "----+")
}

// Dynamically gets all max lengths of struct fields
// A little hacky but I'm a little lazy
func maxes(j []Job) map[string]int {
	m := make(map[string]int)
	if len(j) < 1 {
		return nil
	}

	for x := range j {
		// reflect.Value
		e := reflect.ValueOf(&j[x]).Elem()
		for i := 0; i < e.NumField(); i++ {
			// Type contains a structField
			name := e.Type().Field(i).Name
			// still in reflect.value
			value := e.Field(i).String()
			if m[name] == 0 {
				m[name] = len(value)
				continue
			}
			if len(value) > m[name] {
				m[name] = len(value)
			}
		}
	}

	return m
}

func getJobs() ([]Job, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/jobs", PROJECT_ID), nil)
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GKEY"))
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var jobs []Job
	json.NewDecoder(resp.Body).Decode(&jobs)

	if len(jobs) < 1 {
		return nil, errors.New("No Jobs found")
	}
	return jobs, nil
}
